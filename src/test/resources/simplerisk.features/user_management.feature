Feature: User Management

  User Story:
  In order to manage users
  As an admin
  I need CRUD permissions to user accounts

  Rules:
  - non-admn users have no permissions over other accounts
  - -Therew are 2 types of non-admin accounts:
  -- risk mgrs
  -- asset mgrs
  - non admin users must only be able to see risks or assets in their groups
  - admin must be able to reset user's passwords
  - when an account is created a user must be forced to reset their password on first login

  Questions:
  - do admin users need access to all accounts or only to the accounts in their grp

  To do:
  - force reset of password on account creation

  Domain Language:
  - group = users are defined by the group they belomng to
  - CRUD = create, delete, read, update
  - admin permissions = access to CRUD risks, users and assets for all grps
  - risk_mgmt permissions
  - risk_mgmt ..

  gherkin code follows

  Background:

    Given a user called Simon with administrator permissions and password A!cdefg1
    And a user called tom with risk_management permissions and password A!cdefg1

  @high-impact
  Scenario Outline: The admin checks a user's details
    When Simon is logged in with password A!cdefg1
    Then he is able to view <user>'s account
    Examples:
      | user |
      |tom   |

  @high-risk
  @to-do
  Scenario Outline: a user's password is reset by the admin
    Given a <user> has lost his password
    When Simon can reset <user>'s password
    Then Simon can reset <user>'s password
    Examples:
      | user |
      |tom   |